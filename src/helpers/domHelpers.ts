export function createElement({ tagName, className, id, attributes = {} }: {tagName: string, className?: string, id?: string, attributes?: any}) {
    const element = document.createElement(<string>tagName);

    if (className) {
        const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

    return element;
}