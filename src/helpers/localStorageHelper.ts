
// class for working with local storage in browser (common that can use other classes for store some data)
export class LocalStorageWorker {


    // add value to storage
    static add(key: any, item: string) {
        localStorage.setItem(key, item);
    }

    // get all values from storage (all items)
    // static getAllItems(): Array<StorageItem> {
    //     var list = new Array<StorageItem>();

    //     for (var i = 0; i < localStorage.length; i++) {
    //         var key = localStorage.key(i);
    //         var value = localStorage.getItem(key);

    //         list.push(new StorageItem({
    //             key: key,
    //             value: value
    //         }));
    //     }

    //     return list;
    // }

    // get only all values from localStorage
    static getAllValues(): Array<any> {
        var list = new Array<any>();

        for (var i = 0; i < localStorage.length; i++) {
            var key: any = localStorage.key(i);
            var value = localStorage.getItem(key);

            list.push(value);
        }

        return list;
    }

    // get one item by key from storage
    get(key: string): string {
        var item = localStorage.getItem(key);
        if (!item) return ""
        return item;
    }

    // remove value from storage
    remove(key: string) {
        localStorage.removeItem(key);
    }

    // clear storage (remove all items from it)
    clear() {

        localStorage.clear();

    }
}