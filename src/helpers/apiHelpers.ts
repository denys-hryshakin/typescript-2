async function callApi(endpoint: string, method: string): Promise<[]> {
  const url = endpoint;
  const options = {
    method,
  };

  const response: Promise<[]> = fetch(url, options)
    .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
    .then((result) => {
      return result.results;
    })
    .catch((error) => {
      throw error;
    });

  return response;
}

export { callApi };