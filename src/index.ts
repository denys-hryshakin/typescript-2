import { btn } from "./components/button/btnWrapper";
import { createFavouriteMovies } from "./components/favouriteMovies";
import { createPopularMovies } from "./components/popularMovies";

export async function render(): Promise<void> {
    // TODO render your app here
    createPopularMovies();
    createFavouriteMovies();
    btn.getUpcomingMovies();
    btn.getPopularMovies();
    btn.getTopRatedMovies();
    btn.getSearchMovies();

    localStorage.setItem('id', '508943')
}






