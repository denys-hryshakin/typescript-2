import { createPopularMovies } from "../popularMovies";
import { createSearchMovies } from "../searchMovie";
import { createTopRatedMovies } from "../topRatedMovies";
import { createUpcomingMovies } from "../upcomingMovies";

class BtnWrapper {

    count = 1;

    getPopularMovies() {
        const popular = document.getElementById('popular');

        const onClick = () => {
            const el = document.getElementById('film-container');
            while (el?.firstChild) {
                el?.removeChild(el?.firstChild);
            }
            createPopularMovies();
            this.count = 1;
        }
        this.loadMore(createPopularMovies);

        popular?.addEventListener('click', onClick, false);
    };

    getUpcomingMovies() {
        let count = 1;
        const upcoming = document.getElementById('upcoming');

        const onClick = () => {
            const el = document.getElementById('film-container');
            while (el?.firstChild) {
                el?.removeChild(el?.firstChild);
            }
            createUpcomingMovies(count);
        }

        upcoming?.addEventListener('click', onClick, false);
    };

    getTopRatedMovies() {
        let count = 1;
        const topRated = document.getElementById('top_rated');
        const onClick = () => {
            const el = document.getElementById('film-container');
            while (el?.firstChild) {
                el?.removeChild(el?.firstChild);
            }
            createTopRatedMovies(count);
        }

        topRated?.addEventListener('click', onClick, false);
    };

    // addFavMovie() {
    //     const submit = document.getElementsByClassName('svg');

    //     const onClick = () => {

    //     }
    // };

    getSearchMovies() {
        const submit = document.getElementById('submit');

        const onClick = () => {
            const search = (<HTMLInputElement>document.getElementById('search')).value;
            const el = document.getElementById('film-container');
            while (el?.firstChild) {
                el?.removeChild(el?.firstChild);
            }
            createSearchMovies(search);
        }
        submit?.addEventListener('click', onClick, false);
    };

    loadMore(method: any) {
        const loadMore = document.getElementById('load-more');

        const onClick = () => {
            this.count = this.count + 1;
            method(this.count)
        }

        loadMore?.addEventListener('click', onClick, false);
    }
}

export const btn = new BtnWrapper();
