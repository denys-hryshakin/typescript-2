import { createElement } from "../helpers/domHelpers";
import { TMovie } from "./movie";

export async function createRandomMovie(movies: []) {
    const random = Math.floor(Math.random() * movies.length)

    const url = 'https://image.tmdb.org/t/p/original';
    const { backdrop_path } = movies[random];

    const randomPopularMovie = document.getElementById('random-movie');
    randomPopularMovie?.setAttribute("style", `background-image: url('${url}${backdrop_path}'); background-size: cover; background-position: center; background-repeat: no-repeat;`)

    while (randomPopularMovie?.firstChild) {
        randomPopularMovie?.removeChild(randomPopularMovie?.firstChild);
    }

    const rowElement = createRow(movies[random]);
    randomPopularMovie?.appendChild(rowElement)
}

function createRow(movie: TMovie) {
    const attributes = { style: 'background-color: #2525254f' }

    const rowElement = createElement({ tagName: 'div', className: 'row py-lg-5', attributes })
    const colElement = createElement({ tagName: 'div', className: 'col-lg-6 col-md-8 mx-auto', attributes });
    const titleElement = createTitle(movie);
    const descriptionElement = createDescription(movie);

    colElement.append(titleElement, descriptionElement)
    rowElement.appendChild(colElement);

    return rowElement;
}

function createTitle(movie: TMovie) {
    const { original_title } = movie;
    const attributes = { id: 'random-movie-name', }

    const titleElement = createElement({ tagName: 'h1', className: 'fw-light text-light', attributes })

    titleElement.innerText = original_title;

    return titleElement;
}

function createDescription(movie: TMovie) {
    const { overview } = movie;
    const attributes = { id: 'random-movie-description', }

    const descriptionElement = createElement({ tagName: 'p', className: 'lead text-white', attributes })

    descriptionElement.innerText = overview;

    return descriptionElement;
}


