import { movieService } from "../services/movieService";
import { createMovies } from "./movie";
import { createRandomMovie } from "./randomMovie";

export async function createTopRatedMovies(page?: number) {

    const filmContainer = document.getElementById('film-container');
    const topRatedMovies = await movieService.getTopRatedMovies(page);
    const moviesElement = createMovies(topRatedMovies);

    createRandomMovie(topRatedMovies);

    filmContainer?.append(...moviesElement);
}