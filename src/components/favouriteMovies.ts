import { createElement } from "../helpers/domHelpers";
import { LocalStorageWorker } from "../helpers/localStorageHelper";
import { movieService } from "../services/movieService";
import { TMovie } from "./movie";

export async function createFavouriteMovies() {
    const favouriteMovie = document.getElementById('favorite-movies');
    const movies = await LocalStorageWorker.getAllValues();
    const values: any = [];
    movies.map((movie: any) => {
        const favMovie = movieService.getMovieDetails(movie)
        values.push(favMovie);
    });
    const movieElements = values.map((movie: any) => createMovie(movie));

    favouriteMovie?.append(...movieElements);
}

function createMovie(movie: TMovie) {
    const movieContainer = createElement({ tagName: 'div', className: 'col-12 p-2' });
    const movieElement = createElement({ tagName: 'div', className: 'card shadow-sm' });
    const imageElement = createImage(movie);
    const titleElement = createBody(movie);
    const svgElement = createSvg();

    movieElement.append(imageElement, svgElement, titleElement);
    movieContainer.append(movieElement);

    return movieContainer;
}

function createImage(movie: TMovie) {
    const { poster_path, original_title } = movie;
    const attributes = {
        src: 'https://image.tmdb.org/t/p/original/' + poster_path,
        title: original_title,
        alt: original_title,
    };
    const imgElement = createElement({
        tagName: 'img',
        attributes
    });

    return imgElement;
}

function createSvg() {
    const attributes = {
        xmlns: 'http://www.w3.org/2000/svg',
        stroke: 'red',
        fill: '#ff000078',
        width: '50',
        height: '50',
        viewBox: '0 -2 18 22',
        d: 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z',
    };

    const svgElement = document.createElementNS(attributes.xmlns, "svg");
    svgElement.setAttributeNS(null, "stroke", attributes.stroke);
    svgElement.setAttributeNS(null, "fill", attributes.fill);
    svgElement.setAttributeNS(null, "width", attributes.width);
    svgElement.setAttributeNS(null, "height", attributes.height);
    svgElement.setAttributeNS(null, "class", "bi bi-heart-fill position-absolute p-2")
    svgElement.setAttributeNS(null, "viewBox", attributes.viewBox);

    const path = document.createElementNS(attributes.xmlns, "path");
    path.setAttributeNS(null, "fill-rule", "evenodd");
    path.setAttributeNS(null, "d", attributes.d);

    svgElement.appendChild(path);
    return svgElement;
}


function createBody(movie: TMovie) {
    const { overview, release_date } = movie;

    const cardBodyElement = createElement({ tagName: 'div', className: 'card-body' });
    const titleElement = createElement({ tagName: 'p', className: 'card-text truncate' });

    const alignBlock = createElement({ tagName: 'div', className: 'd-flex justify-content-between align-items-center' });
    const releaseDateElement = createElement({ tagName: 'small', className: 'text-muted' });

    titleElement.innerText = overview;
    releaseDateElement.innerText = release_date;

    alignBlock.append(releaseDateElement);
    cardBodyElement.append(titleElement, alignBlock);

    return cardBodyElement;
}
