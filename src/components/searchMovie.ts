import { movieService } from "../services/movieService";
import { searchService } from "../services/searchService";
import { createMovies } from "./movie";
import { createRandomMovie } from "./randomMovie";

export async function createSearchMovies(keyword: any) {

    const filmContainer = document.getElementById('film-container');
    const searchMovies = await searchService.searchMovie(keyword);
    const moviesElement = createMovies(searchMovies);

    createRandomMovie(searchMovies);

    filmContainer?.append(...moviesElement);
}