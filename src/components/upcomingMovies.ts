import { movieService } from "../services/movieService";
import { createMovies } from "./movie";
import { createRandomMovie } from "./randomMovie";

export async function createUpcomingMovies(page?: number) {

    const filmContainer = document.getElementById('film-container');
    const upcomingMovies = await movieService.getUpcomingMovies(page);
    const moviesElement = createMovies(upcomingMovies);

    createRandomMovie(upcomingMovies);
    filmContainer?.append(...moviesElement);
}