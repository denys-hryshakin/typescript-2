import { movieService } from "../services/movieService";
import { createMovies } from "./movie";
import { createRandomMovie } from "./randomMovie";

export async function createPopularMovies(page?: number) {

    const filmContainer = document.getElementById('film-container');
    const popularMovies = await movieService.getPopularMovies(page);
    const moviesElement = createMovies(popularMovies);

    createRandomMovie(popularMovies);
    filmContainer?.append(...moviesElement);


}