import { callApi } from "../helpers/apiHelpers";

class MovieService {

    API_URL = 'https://api.themoviedb.org/3/movie/';
    API_KEY = 'c7158594367254b2ed2f2ac72ff42a1b';

    async getPopularMovies(page?: number): Promise<[]> {
        try {
            const endpoint = `${this.API_URL}popular?api_key=${this.API_KEY}&page=${page}`;
            const apiResult = await callApi(endpoint, 'GET')

            return apiResult;
        } catch (error) {
            throw error;
        }
    };

    async getUpcomingMovies(page?: number): Promise<[]> {
        try {
            const endpoint = `${this.API_URL}upcoming?api_key=${this.API_KEY}&page=${page}`;
            const apiResult = await callApi(endpoint, 'GET')

            return apiResult;
        } catch (error) {
            throw error;
        }
    };

    async getTopRatedMovies(page?: number): Promise<[]> {
        try {
            const endpoint = `${this.API_URL}top_rated?api_key=${this.API_KEY}&page=${page}`;
            const apiResult = await callApi(endpoint, 'GET')

            return apiResult;
        } catch (error) {
            throw error;
        }
    };

    async getMovieDetails(id: string): Promise<[]> {
        try {
            const endpoint = `${this.API_URL}${id}?api_key=${this.API_KEY}`;
            const apiResult = await callApi(endpoint, 'GET')
            console.log(apiResult)
            return apiResult;
        } catch (error) {
            throw error;
        }
    };

}

export const movieService = new MovieService();