import { callApi } from "../helpers/apiHelpers";

class SearchService {

    API_URL = 'https://api.themoviedb.org/3/search/';
    API_KEY = 'c7158594367254b2ed2f2ac72ff42a1b';

    async searchMovie(keyword: string): Promise<[]> {
        try {
            const endpoint = `${this.API_URL}movie?api_key=${this.API_KEY}&page=1&query='${keyword}'`;
            const apiResult = await callApi(endpoint, 'GET')

            return apiResult;
        } catch (error) {
            throw error;
        }
    };

}

export const searchService = new SearchService();